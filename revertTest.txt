I found this limerick online
It was funny, have a look:
There was a small boy of Quebec,
Who was buried in snow to his neck;
When they said. "Are you friz?"
He replied, "Yes, I is—
But we don't call this cold in Quebec." -A limerick by Rudyard Kipling
I always liked limericks
This one was nice

