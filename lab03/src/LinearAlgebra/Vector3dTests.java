//Luke Weaver 1937361
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void vectorCreateTest() {
		Vector3d v = new Vector3d(1.0, 2.0, 3.0);
		
		assertEquals(1.0, v.getX());
		assertEquals(2.0, v.getY());
		assertEquals(3.0, v.getZ());
	}
	
	@Test
	void magnitudeTest() {
		Vector3d v = new Vector3d(1.0, 2.0, 3.0);
		
		assertEquals(Math.sqrt(14), v.magnitude());
	}
	
	@Test
	void dotProductTest() {
		Vector3d v1 = new Vector3d(1.0, 1.0, 2.0);
		Vector3d v2 = new Vector3d(2.0, 3.0, 4.0);
		
		assertEquals(13.0, v1.dotProduct(v2));
	}
	
	@Test
	void addTest() {
		Vector3d v1 = new Vector3d(1.0, 1.0, 2.0);
		Vector3d v2 = new Vector3d(2.0, 3.0, 4.0);
		Vector3d v3 = v1.add(v2);
		
		assertEquals(3.0, v3.getX());
		assertEquals(4.0, v3.getY());
		assertEquals(6.0, v3.getZ());
	}
}
