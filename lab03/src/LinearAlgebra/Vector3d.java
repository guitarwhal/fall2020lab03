//Luke Weaver 1937361
package LinearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double x, double y, double z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double getX(){
		return this.x;
	}
	
	public double getY(){
		return this.y;
	}
	
	public double getZ(){
		return this.z;
	}
	
	public double magnitude(){
		return Math.sqrt(Math.pow(this.x,2) + Math.pow(this.y,2) + Math.pow(this.z,2));
	}
	
	public double dotProduct(Vector3d v){
		return (this.x * v.x) + (this.y * v.y) + (this.z * v.z);
	}
	
	public Vector3d add(Vector3d v) {
		Vector3d newV = new Vector3d((this.x + v.x),(this.y + v.y),(this.z + v.z));
		return newV;
	}
}